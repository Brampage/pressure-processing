import processing.serial.*; 

String serial;                 // Declare a new string called 'serial' . A string is a sequence of characters (data type know as "char")
Serial port;                   // The serial port, this is a new instance of the Serial class (an Object)
int end = 10;                  // the number 10 is ASCII for linefeed (end of serial.println), later we will look for this to break up individual messages

/*
  The sensors:
 sensorValues[1] is Pressure Sensor left foot
 sensorValues[0] is Pressure Sensor right foot
 */
int[] sensorValues;

// Constants
int BACKGROUND = 1;

// Canvas variables
PShape verticalLine;
PShape horizontalLine;
PShape foot;
color LINECOLOR = color(0, 0, 255);
int ELLIPSEWIDTH = 200;

void setup() {
  printArray(Serial.list());                         // List connected devices 
  port = new Serial(this, Serial.list()[2], 9600);   // Initializing the object by assigning a port and baud rate (must match that of Arduino)
  port.clear();                                      // Function from serial library that throws out the first reading, in case we started reading in the middle of a string from Arduino
  serial = port.readStringUntil(end);                // Function that reads the string from serial port until a println and then assigns string to our string variable (called 'serial')
  serial = null;                                     // Initially, the string will be null (empty)

  // Processing setup settings
  size(800, 600); // Params width, height
  frameRate(30);
  background(BACKGROUND);

  shapeMode(CENTER);
  foot = createShape();
  foot.beginShape();
  foot.vertex(36.5, 0);
  foot.vertex(73, 49);
  foot.vertex(73, 180);
  foot.vertex(0, 180);
  foot.vertex(0, 49);
  foot.stroke(LINECOLOR);
  foot.endShape(CLOSE);
}

void draw() {
  getSensorValues();

  // Only draw, if there are sensorValues
  if (sensorValues != null) {
    background(BACKGROUND);
    showCircles(sensorValues[1], sensorValues[0]);
    drawFeet(sensorValues[1], sensorValues[0]);
  }

  delay(2000);

  save("pressure-" + hour() + "-" + minute() + "-" + second() + ".png");
}

void showCircles(int leftFootValue, int rightFootValue) {
  stroke(LINECOLOR);
  float leftFootAlpha = map(leftFootValue, 0, 1000, 0, 255);
  float rightFootAlpha = map(rightFootValue, 0, 1000, 0, 255);

  fill(#FF0000, leftFootAlpha);
  ellipse((width / 2) / 2, (height / 2) / 2, ELLIPSEWIDTH, ELLIPSEWIDTH);
  fill(#FF0000, rightFootAlpha);
  ellipse((width / 2) + (width / 2 / 2), (height / 2) / 2, ELLIPSEWIDTH, ELLIPSEWIDTH);
}

void drawFeet(int leftFootValue, int rightFootValue) {
  // Optional: replace by loadShape() (SVG file)
  float leftFootMappedValue = map(leftFootValue, 0, 1000, 0, (height / 2));
  float rightFootMappedValue = map(rightFootValue, 0, 1000, 0, (height / 2));

  shape(foot, (width / 2) / 2, (height / 2) / 2 + 210, leftFootMappedValue, leftFootMappedValue);
  shape(foot, (width / 2) + (width / 2 / 2), (height / 2) / 2 + 210, rightFootMappedValue, rightFootMappedValue);
}


void getSensorValues() {
  while (port.available() > 0) { 
    serial = port.readStringUntil(end);
  }
  if (serial != null) {
    sensorValues = int(splitTokens(serial));
  }
}

void printSensorValues() {
  if (sensorValues != null) {
    println(sensorValues);
    println("------------------");
  }
}